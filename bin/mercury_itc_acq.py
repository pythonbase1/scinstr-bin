#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""package mercury
Created on Fri Dec 12 13:52:23 2013
mercury_acq.py
This program is made to communicate with cryogenic controller
Oxford Mercury.
@author: Christophe Fluhr
"""

import os
import sys
import argparse
import time

DEVICE_IP = '169.254.0.10'
DEVICE_PORT = 7020
STEP_TIME = 1  # Step time acquisition
OFILENAME = 'data-mercury' # File header
IN_SLOT = 8  # default input slot
VAL_TYPE = 'TEMP' # default value type


#==============================================================================
def parse():
    """ Specific parsing procedure for transfering data from Mercury
    controller.
    Return parsed arguments.
    """
    parser = argparse.ArgumentParser( \
        description='Acquire data from Mercury iTC controller connected '\
        'through Ethernet.',
        epilog='Example: \'mercury_acq -o toto -s 8 -t 2 \' configure ' \
        'mercury_acq with output file toto-"date".dat, input slot 8 DC '\
        'and step time acquisition 2 seconds.')
    parser.add_argument('-o', action='store', dest='ofile', \
                        default=OFILENAME,\
                        help='Output data filename (default '+OFILENAME+')')
    parser.add_argument('-s', action='store', dest='input_slot', \
                        default=IN_SLOT,\
                        help='Input slot 6,7 or 8 (default '+str(IN_SLOT)+')')
    parser.add_argument('-t', action='store', dest='steptime', \
                        default=STEP_TIME,\
                        help='Step time acquistion'\
                        ' (default '+str(STEP_TIME)+' second)')
    parser.add_argument('-ip', action='store', dest='ip', \
                        default=DEVICE_IP, help='IP address of controller'\
                        ' (default '+str(DEVICE_IP)+')')
    parser.add_argument('-po', action='store', dest='port', \
                        default=DEVICE_PORT, help='Port of the controller'\
                        ' (default '+str(DEVICE_PORT)+')')
    parser.add_argument('-v', action='store', dest='vtype', \
                        default=VAL_TYPE, help='Value type TEMP or RES'\
                        ' (default '+VAL_TYPE+')')
    args = parser.parse_args()
    return args


#==============================================================================
def connect(ip,port):
    """Creation of a socket to establish the communication
    with Mercury temperature controller"""
    try:
        print 'Mercury iTC connection state at "%s" ?' % ip
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, \
        socket.IPPROTO_TCP)
        sock.settimeout(5) 		# Don't hang around forever
        sock.connect((ip, port)) # Start the connection
        print '  --> Connected'
        return sock
    except socket.error as ex: # Debugging
        if 'Connection refused' in ex:
            print 'Wrong PORT (Default PORT : 7020)\n', ex
            print '\n  --> Not connected'
        elif 'No route to host' in ex:
            print 'Wrong address (Default address : 192.168.0.10)\n', ex
            print '\n  --> Not connected'
        else:
            print ex+'\n'+'  --> Not connected'
        sys.exit()


#==============================================================================
def init_mercury(sock):
    """Initialization of the controller ??????
    """
    p_val = [0, 0, 0]
    i_val = [0, 0, 0]
    d_val = [0, 0, 0]
    setpoint = [0, 0, 0]
    for i in range(0, 3):
        sock.send('READ:DEV:DB' + str(i + 6) + '.T1:TEMP:LOOP:P\n')
        p_read = read_buffer(sock)
        p_val[i] = float(p_read[28:])
        sock.send('READ:DEV:DB' + str(i + 6) + '.T1:TEMP:LOOP:I\n')
        i_read = read_buffer(sock)
        i_val[i] = float(i_read[28:])
        sock.send('READ:DEV:DB' + str(i + 6) + '.T1:TEMP:LOOP:D\n')
        d_read = read_buffer(sock)
        d_val[i] = float(d_read[28:])
        sock.send('READ:DEV:DB' + str(i + 6) + '.T1:TEMP:LOOP:TSET\n')
        setpoint_read = read_buffer(sock)
        setpoint[i] = float(setpoint_read[31:len(setpoint_read)-2])
    return (p_val, i_val, d_val, setpoint)


#==============================================================================
def check_error(sock):
    """Used for debugging during the development of the program,
    not used anymore
    """
    sock.send('SYST:ERR?\n')
    error = None
    try:
        error = sock.recv(128)
    except socket.timeout:
        error = ""
    print error


#==============================================================================
def read_buffer(sock):
    """Read the data returned by Mercury iTC until the '\n
    '"""
    ans = ''
    nb_data_list = []
    nb_data = ''
    while ans != '\n':
        ans = sock.recv(1)
        nb_data_list.append(ans)  # Return the number of data
    list_size = len(nb_data_list)
    for j in range (0, list_size):
        nb_data = nb_data+nb_data_list[j]
    return(nb_data)  # Return the number of data in the memory


#==============================================================================
def set_pid_mercury(sock,pset,iset,dset):
    """Control loop setting
    """
    slot_choice = ''
    while slot_choice != '6' and slot_choice != '7' and slot_choice != '8':
        slot_choice = input('\n Setpoint for wich slot? (6,7 or 8)\n')
    slot = int(slot_choice)
    _choice = input('\n Enter P value (0-199.9), current is ' +
                    str(pset[slot-6]) + ' ):\n')
    pset[slot-6] = float(p_choice) #Save new value
    command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:LOOP:P:' \
        + str(pset[slot-6]) + '\n'
    ##print command
    sock.send(command) # Send new P value
    #
    i_choice = input('\n Enter I value (0-199.9), current is ' +
                     str(iset[slot-6]) + ' ):\n')
    iset[slot-6] = float(i_choice)  #Save new value
    command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:LOOP:I:' \
        + str(iset[slot-6]) + '\n'
    sock.send(command) # Send new I value
    #
    d_choice = input('\n Enter D value (0-299.9), current is ' +
                     str(dset[slot-6]) + ' ):\n')
    dset[slot-6] = float(d_choice) #Save new value
    command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:LOOP:D:' \
        + str(dset[slot-6]) + '\n'
    sock.send(command) # Send new D value
    #
    return (pset, iset, dset)


#==============================================================================
def setpoint_mercury(sock,setpoint):
    """Control loop setting
    """
    slot_choice = ''
    while slot_choice !=  '6' and slot_choice !=  '7' and slot_choice !=  '8':
        slot_choice = input('\n Setpoint for wich slot? (6,7 or 8)\n')
    slot = int(slot_choice)
    setpoint_choice = input('\n Enter setpoint (K), current is ' + \
                            str(setpoint[slot-6]) + ' ):\n')
    setpoint[slot-6] = float(setpoint_choice)  # Save new value
    command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:LOOP:TSET:' \
        + str(setpoint[slot-6]) + '\n'
    sock.send(command)  # Send new P value
    read_buffer(sock)  # Empty the buffer
    loop_choice = ''
    while loop_choice != 'y' and loop_choice != 'n':
        loop_choice = raw_input('\n Enable control loop for this sensor y or n?\n')

    if loop_choice == 'y':
        command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:ENAB:ON\n'
        sock.send(command)  # Send new P value
        read_buffer(sock)  # Empty the buffer
    elif loop_choice == 'n':
        command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:ENAB:OFF\n'
        sock.send(command)  # Send new P value
        read_buffer(sock)  # mpty the buffer
        command = 'SET:DEV:DB' + str(slot) + '.T1:TEMP:HSET:0\n'
        sock.send(command)  # Send new P value
        read_buffer(sock)  # Empty the buffer
    else:
        pass
    return setpoint


#==============================================================================
def acqu_mercury(i,sock, data_file, steptime, input_slot, vtype,
                 pset, iset, dset, setpoint):
    """Temperature acquisition and stockage in a file
    """
    print "Waiting for the first acquisition"
    command_slot6 = 'READ:DEV:DB6.T1:TEMP:SIG:' + vtype + '\n'
    command_slot7 = 'READ:DEV:DB7.T1:TEMP:SIG:' + vtype + '\n'
    command_slot8 = 'READ:DEV:DB8.T1:TEMP:SIG:' + vtype + '\n'
    while True:
        try:
             try:
                i = i + 1  # To check the number of data already read
                date = time.mktime(time.localtime())
                #time of acquistion's point
                sock.send(command_slot6)
                read_slot6 = read_buffer(sock)
                sock.send(command_slot7)
                read_slot7 = read_buffer(sock)
                sock.send(command_slot8)
                read_slot8 = read_buffer(sock)
                if vtype == 'RES':
                     temperature_slot6 = read_slot6[29:len(read_slot6)-1]  # Use only the value
                     temperature_slot7 = read_slot7[29:len(read_slot7)-1]  # Use only the value
                     temperature_slot8 = read_slot8[29:len(read_slot8)-1]  # Use only the value
                else:
                    temperature_slot6 = read_slot6[30:len(read_slot6)-2]  # Use only the value
                    temperature_slot7 = read_slot7[30:len(read_slot7)-2]  # Use only the value
                    temperature_slot8 = read_slot8[30:len(read_slot8)-2]  # Use only the value
                str_date = str(time.strftime("%H%M%S",time.localtime(date)))
                sample = str(i) + '\t' + temperature_slot6 + '\n'
                data_file.write(sample) # Write in a file
                print sample
                time.sleep(int(steptime)) # Wait the time of the gate time
             except Exception as ex:
                print 'Exception during controler data reading: ' + str(ex)
        except KeyboardInterrupt:
            keyboard = input('\n Q to quit, P to change PID parameters' \
                             ', S to change setpoint or another key to continue\n')
            if keyboard == 'q':
               break  # To stop the loop in a clean way
            elif keyboard == 'p':
               (pset, iset, dset) = set_pid_mercury(sock,pset,iset,dset)
               sample = '#News PID values:\n #P=' + str(pset) + '\t I=' \
                   + str(iset) + '\t D=' + str(dset) + '\n'
               data_file.write(sample)  # Write in a file
               print sample
               continue
            elif keyboard == 's':
                setpoint = setpoint_mercury(sock,setpoint)
                sample = '#New setpoint values:\n#DB6=' + str(setpoint[0]) \
                    + 'K\t DB7=' + str(setpoint[1]) \
                    + 'K\t DB8=' + str(setpoint[2]) + 'K\n'
                data_file.write(sample)  # Write in a file
                print  sample
                continue
            else:
                continue


#==============================================================================
def main():
    """Main program
    """
    args = parse()  # Parse command line
    ofile = args.ofile  # Data output file
    ip = args.ip
    input_slot = args.input_slot
    steptime = args.steptime
    port = args.port
    vtype= args.vtype
    sock = connect(ip, port)
    print '\n connnected'
    date = time.mktime(time.localtime())
    filename = ofile + '-' + time.strftime("%d%m%Y-%H%M%S",
                                           time.localtime(date)) + '.dat'
    # "experiment"+"date and hour".dat
    data_file = open(filename, 'wr', 0) # Create the file in write and read
    # mode, the file is updated at each sample
    data_file.write('# Number of the sample-temperature -'
                    'date when the data is computed\n')

    (pset, iset, dset, setpoint) = init_mercury(sock)  # init PID values

    print pset
    print iset
    print dset
    print setpoint

    # Temperature acquisition
    i=0
    acqu_mercury(i, sock, data_file, steptime, input_slot, vtype,
                 pset, iset, dset, setpoint)

    sock.close()
    print '\n  --> Disconnected'
    data_file.close()
    try:
        ans = input('Would you like to keep this datafile'
                    '(y/n : default y)?\t')
        if ans == 'n':
            os.remove(filename)
            print '\n', filename, 'removed\n'
        else:
            print '\n', filename, 'saved\n'
    except Exception as ex:
        print 'Oups '+str(ex)
    print 'Program ending\n'


#==============================================================================
if __name__ == "__main__":
    main()
